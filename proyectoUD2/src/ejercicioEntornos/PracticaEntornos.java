package ejercicioEntornos;

import java.util.Scanner;

public class PracticaEntornos {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena = lector.nextLine();
		
		System.out.println("Introduce otra cadena");
		String cadena2 = lector.nextLine();
		
		System.out.println("Esta es la longitud de la cadena 1 = "+cadena.length());
		System.out.println("Esta es la longitud de la cadena 2 = "+cadena2.length());

		for (int i = 0; i < cadena.length(); i++) {
			System.out.println("El caracter "+i+" de la cadena 1 es "+cadena.charAt(i));
			System.out.println("hdomth");
		}
		
		if (cadena.compareTo(cadena2)==0) {
			System.out.println("Son iguales");
		} else {
			System.out.println("No son iguales");
		}
		
		
		lector.close();
	}

}
